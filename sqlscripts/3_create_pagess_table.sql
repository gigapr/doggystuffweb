CREATE TABLE IF NOT EXISTS public.pages
(
    page_id serial,
    title character varying(350) COLLATE pg_catalog."default" NOT NULL,
    body character varying COLLATE pg_catalog."default" NOT NULL,
    date_created timestamp without time zone,
    CONSTRAINT page_pkey PRIMARY KEY (page_id),
    CONSTRAINT pages_title_key UNIQUE (title)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.pages
    OWNER to doggystuffwebuser;
    