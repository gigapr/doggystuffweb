CREATE DATABASE doggystuffweb
    WITH 
    OWNER = gsantonastaso
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.UTF-8'
    LC_CTYPE = 'en_US.UTF-8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

GRANT TEMPORARY, CONNECT ON DATABASE doggystuffweb TO PUBLIC;

GRANT ALL ON DATABASE doggystuffweb TO doggystuffwebuser;
