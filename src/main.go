package main

import (
	"doggystuffweb/src/controllers"
	"doggystuffweb/src/events"
	"doggystuffweb/src/middlewares"
	"doggystuffweb/src/repositories"
	"doggystuffweb/src/utils"

	"log"
	"net/http"

	_ "github.com/lib/pq"
)

func main() {

	settings := InitialiseSetting()

	db := repositories.ConnectToDatabase(settings.Host, settings.Database, settings.Port, settings.User, settings.Password)
	defer db.Close()

	templates := utils.GetAllTemplates("./templates/")

	controllers.Startup(templates, events.NewEventsPlublisher(settings.DynamoDbEventsRegion, settings.DynamoDbEventsTable))

	log.Println("Listening on", settings.Addr)

	authenticationMiddleware := new(middlewares.AuthenticationMiddleware)
	authenticationMiddleware.UrlsThatDontRequireAuthentication = settings.UrlsThatDontRequireAuthentication

	timeoutMiddleware := new(middlewares.TimeoutMiddleware)
	timeoutMiddleware.Next = authenticationMiddleware

	gZipMiddleware := new(middlewares.GZipMiddleware)
	gZipMiddleware.Next = timeoutMiddleware
	gZipMiddleware.Exclude = settings.GZipExclude

	log.Fatal(http.ListenAndServe(settings.Addr, gZipMiddleware))
}
