package main

import (
	"os"
)

type Setting struct {
	Host                              string
	Database                          string
	Port                              string
	User                              string
	Password                          string
	Addr                              string
	DynamoDbEventsTable               string
	DynamoDbEventsRegion              string
	UrlsThatDontRequireAuthentication []string
	GZipExclude                       []string
}

func InitialiseSetting() *Setting {
	settings := new(Setting)
	settings.Host = os.Getenv("POSTGRES_HOST")
	settings.Database = os.Getenv("POSTGRES_DATABASE")
	settings.Port = os.Getenv("POSTGRES_PORT")
	settings.User = os.Getenv("POSTGRES_USER")
	settings.Password = os.Getenv("POSTGRES_PASSWORD")
	settings.Addr = ":" + os.Getenv("WEBSITE_PORT")
	settings.DynamoDbEventsTable = os.Getenv("DYNAMODB_EVENTS_TABLE")
	settings.DynamoDbEventsRegion = os.Getenv("DYNAMODB_REGION")
	settings.UrlsThatDontRequireAuthentication = []string{"signup", "login", "metrics"}
	settings.GZipExclude = []string{"metrics"}
	return settings
}
