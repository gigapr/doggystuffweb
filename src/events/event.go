package events

import (
	"time"

	"github.com/nu7hatch/gouuid"
)

type Event struct {
	Id          string    `json:"id"`
	EventType   string    `json:"eventType"`
	Data        string    `json:"data"`
	Version     int       `json:"version"`
	CreatedDate time.Time `json:"createdDate"`
}

func NewEvent(eventType string, data string, version int) Event {
	u, err := uuid.NewV4()
	if err != nil {
		panic(err)
	}
	event := Event{
		Id:          u.String(),
		EventType:   eventType,
		Data:        data,
		Version:     version,
		CreatedDate: time.Now().UTC(),
	}

	return event
}
