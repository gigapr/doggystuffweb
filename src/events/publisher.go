package events

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type EventsPlublisher struct {
	Region    string
	TableName string
}

func NewEventsPlublisher(region string, tableName string) *EventsPlublisher {

	plublisher := new(EventsPlublisher)

	plublisher.Region = region
	plublisher.TableName = tableName

	return plublisher
}

func (r *EventsPlublisher) PublishMessage(event Event) {

	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(r.Region), //credential are read from env variables
	})

	svc := dynamodb.New(sess)

	av, err := dynamodbattribute.MarshalMap(event)

	if err != nil {
		fmt.Println("Got error marshalling map:")
		panic(err)
	}

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(r.TableName),
	}

	_, err = svc.PutItem(input)

	if err != nil {
		panic(err)
	}
}
