package middlewares

import (
	"compress/gzip"
	"doggystuffweb/src/utils"
	"io"
	"net/http"
	"strings"
)

type GZipMiddleware struct {
	Next    http.Handler
	Exclude []string
}

func (gm *GZipMiddleware) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	if utils.Contains(gm.Exclude, r.URL.Path) {
		gm.Next.ServeHTTP(w, r)
		return
	}

	if gm.Next == nil {
		gm.Next = http.DefaultServeMux
	}

	encodings := r.Header.Get("Accept-Encoding")
	if !strings.Contains(encodings, "gzip") {
		gm.Next.ServeHTTP(w, r)
		return
	}
	w.Header().Add("Content-Encoding", "gzip")
	gzipwriter := gzip.NewWriter(w)
	defer gzipwriter.Close()
	grw := gzipResponseWriter{
		ResponseWriter: w,
		Writer:         gzipwriter,
	}
	gm.Next.ServeHTTP(grw, r)
}

type gzipResponseWriter struct {
	http.ResponseWriter
	io.Writer
}

func (grw gzipResponseWriter) Write(data []byte) (int, error) {
	return grw.Writer.Write(data)
}
