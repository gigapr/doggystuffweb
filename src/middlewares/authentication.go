package middlewares

import (
	"doggystuffweb/src/utils"
	"fmt"
	"net/http"
)

type AuthenticationMiddleware struct {
	UrlsThatDontRequireAuthentication []string
	Next                              http.Handler
}

func (tm *AuthenticationMiddleware) ServeHTTP(w http.ResponseWriter, request *http.Request) {
	if tm.Next == nil {
		tm.Next = http.DefaultServeMux
	}

	fmt.Println(request.URL.Path)
	if utils.Contains(tm.UrlsThatDontRequireAuthentication, request.URL.Path) {
		tm.Next.ServeHTTP(w, request)
	}

	userName := utils.GetUserName(request)

	if userName == "" {
		http.Redirect(w, request, fmt.Sprintf("/login"), http.StatusFound)
	} else {
		tm.Next.ServeHTTP(w, request)
	}
}
