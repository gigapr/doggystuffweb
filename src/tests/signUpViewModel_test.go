package tests

import (
	"doggystuffweb/src/viewmodels"
	"testing"
)

func Test_SignUpViewModel_isvalid_return_false_when_email_is_invalid(t *testing.T) {
	emails := []string{
		"Hotmail.com",
		"gigapr@hotmail",
		"@hotmail",
	}

	for _, email := range emails {

		signUpViewModel := viewmodels.SignUpViewModel{
			Email: email,
		}

		if signUpViewModel.IsValid() == true {
			t.Errorf("IsValid should be invalid since '%v' is not in a valid format", signUpViewModel.Email)
		}
	}
}

func Test_SignUpViewModel_isvalid_return_false_when_password_is_missing(t *testing.T) {

	signUpViewModel := viewmodels.SignUpViewModel{
		FirstName:            "MrG",
		LastName:             "Super",
		Email:                "valid@email.com",
		ConfirmationPassword: "password",
	}

	if signUpViewModel.IsValid() == true {
		t.Error("IsValid should be false since the Password value is not set")
	}
}

func Test_SignUpViewModel_isvalid_return_false_when_confirmpassword_is_missing(t *testing.T) {

	signUpViewModel := viewmodels.SignUpViewModel{
		FirstName: "MrG",
		LastName:  "Super",
		Email:     "valid@email.com",
		Password:  "password",
	}

	if signUpViewModel.IsValid() == true {
		t.Error("IsValid should be false since the ConfirmationPassword value is not set")
	}
}

func Test_SignUpViewModel_isvalid_return_false_when_password_and_confirmpassword_dont_match(t *testing.T) {

	signUpViewModel := viewmodels.SignUpViewModel{
		FirstName:            "MrG",
		LastName:             "Super",
		Email:                "valid@email.com",
		Password:             "Password",
		ConfirmationPassword: "ConfirmationPassword",
	}

	if signUpViewModel.IsValid() == true {
		t.Error("IsValid should be false since the Password and ConfirmationPassword value are different")
	}
}
