package tests

import (
	"doggystuffweb/src/viewmodels"
	"testing"
)

func Test_LoginViewModel_is_invalid_when_username_is_missing(t *testing.T) {
	loginViewMode := viewmodels.LoginViewModel{
		Password: "SomePassword",
	}

	if loginViewMode.IsValid() == true {
		t.Errorf("IsValid should be false when Username is missing")
	}
}

func Test_Test_LoginViewModel_is_invalid_when_password_is_missing(t *testing.T) {
	loginViewMode := viewmodels.LoginViewModel{
		Username: "SomeUsername",
	}

	if loginViewMode.IsValid() == true {
		t.Errorf("IsValid should be false when Password is missing")
	}
}

func Test_LoginViewModel_is_valid_when_username_and_password_are_set(t *testing.T) {
	loginViewMode := viewmodels.LoginViewModel{
		Username: "SomeUsername",
		Password: "SomePassword",
	}

	if loginViewMode.IsValid() == false {
		t.Errorf("IsValid should be true when Username and Password are set")
	}
}
