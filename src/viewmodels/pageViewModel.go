package viewmodels

import (
	"time"
)

type PageViewModel struct {
	ID            int
	Title         string
	Body          string
	DateCreated   time.Time
	UserViewModel *UserViewModel
}
