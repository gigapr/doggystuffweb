package viewmodels

import (
	"doggystuffweb/src/utils"
	"net/http"
)

type UserViewModel struct {
	Username   string
	IsLoggedIn bool
}

func NewUserViewModel(request *http.Request) *UserViewModel {

	userName := utils.GetUserName(request)
	userViewModel := new(UserViewModel)

	if userName != "" {
		userViewModel.Username = userName
		userViewModel.IsLoggedIn = true
	} else {
		userViewModel.IsLoggedIn = false
	}

	return userViewModel
}
