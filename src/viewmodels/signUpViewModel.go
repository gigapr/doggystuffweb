package viewmodels

import (
	"regexp"
	"strings"
)

type SignUpViewModel struct {
	FirstName            string
	LastName             string
	Email                string
	Password             string
	ConfirmationPassword string
	Errors               map[string]string
	UserViewModel        *UserViewModel
}

func (msg *SignUpViewModel) IsValid() bool {
	msg.Errors = make(map[string]string)

	re := regexp.MustCompile(".+@.+\\..+")
	matched := re.Match([]byte(msg.Email))
	if matched == false {
		msg.Errors["Email"] = "Please enter a valid email address"
	}

	if strings.TrimSpace(msg.Password) == "" {
		msg.Errors["Password"] = "Please write a Password"
	}

	if strings.TrimSpace(msg.ConfirmationPassword) == "" {
		msg.Errors["ConfirmPassword"] = "Please write the confirmation Password"
	}

	if msg.Password != msg.ConfirmationPassword {
		msg.Errors["ConfirmPassword"] = "Confirmation Password is different from password"
	}

	return len(msg.Errors) == 0
}
