package viewmodels

type PagesViewModel struct {
	Pages         []PageViewModel
	UserViewModel *UserViewModel
}
