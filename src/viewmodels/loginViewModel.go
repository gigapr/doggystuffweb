package viewmodels

type LoginViewModel struct {
	Username      string
	Password      string
	Errors        map[string]string
	UserViewModel *UserViewModel
}

func (msg *LoginViewModel) IsValid() bool {
	msg.Errors = make(map[string]string)

	if msg.Username == "" {
		msg.Errors["Username"] = "Please enter a valid Username"
	}

	if msg.Password == "" {
		msg.Errors["Password"] = "Please enter a valid Password"
	}

	return len(msg.Errors) == 0
}
