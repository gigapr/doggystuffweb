package controllers

import (
	"doggystuffweb/src/mappers"
	"doggystuffweb/src/repositories"
	"doggystuffweb/src/viewmodels"
	"fmt"
	"html/template"
	"net/http"
	"path"
	"strconv"
)

type viewPage struct {
	viewPageTemplate *template.Template
}

func (v viewPage) registerReoutes() {
	http.HandleFunc("/view/", v.handleViewPage)
}

func (v viewPage) handleViewPage(w http.ResponseWriter, request *http.Request) {
	pageID, _ := strconv.Atoi(path.Base(request.URL.Path))
	err, pageDTO := repositories.GetPage(pageID)
	if err != nil {
		http.Redirect(w, request, fmt.Sprintf("/edit/%b", pageID), http.StatusFound)
		return
	}

	w.Header().Add("Content-Type", "text/html")

	pageViewModel := mappers.Map(pageDTO)
	pageViewModel.UserViewModel = viewmodels.NewUserViewModel(request)

	err = v.viewPageTemplate.Execute(w, pageViewModel)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
