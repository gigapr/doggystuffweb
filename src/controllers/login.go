package controllers

import (
	"doggystuffweb/src/repositories"
	"doggystuffweb/src/utils"
	"doggystuffweb/src/viewmodels"
	"html/template"
	"net/http"
)

type loginPage struct {
	loginPageTemplate *template.Template
}

func (l loginPage) registerReoutes() {
	http.HandleFunc("/login", l.handleLogin)
	http.HandleFunc("/loginPost", l.handleLoginPost)
	http.HandleFunc("/logout", l.handleLogout)
}

func (l loginPage) handleLogout(response http.ResponseWriter, request *http.Request) {

	utils.ClearSessionCookie(response)

	http.Redirect(response, request, "/", http.StatusFound)
}

func (l loginPage) handleLogin(w http.ResponseWriter, r *http.Request) {

	w.Header().Add("Content-Type", "text/html")

	err := l.loginPageTemplate.Execute(w, nil)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (l loginPage) handleLoginPost(w http.ResponseWriter, request *http.Request) {

	w.Header().Add("Content-Type", "text/html")

	loginViewModel := &viewmodels.LoginViewModel{
		Username:      request.FormValue("username"),
		Password:      request.FormValue("password"),
		UserViewModel: viewmodels.NewUserViewModel(request),
	}

	if loginViewModel.IsValid() {

		err, passwordFromDb := repositories.GetUserPassword(loginViewModel.Username)

		if err != nil {
			l.showIncorrectCredentialsError(loginViewModel, w)
		} else {
			if utils.PasswordIsValid(passwordFromDb, loginViewModel.Password) {
				l.showIncorrectCredentialsError(loginViewModel, w)
			} else {
				utils.SetSession(loginViewModel.Username, w)
				http.Redirect(w, request, "/", http.StatusFound)
			}
		}
	} else {
		l.loginPageTemplate.Execute(w, loginViewModel)
	}
}

func (l loginPage) showIncorrectCredentialsError(viewmodel *viewmodels.LoginViewModel, w http.ResponseWriter) {
	viewmodel.Errors["IncorrectCredentials"] = "Invalid Username Password combination"
	err := l.loginPageTemplate.Execute(w, viewmodel)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
