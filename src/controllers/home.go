package controllers

import (
	"doggystuffweb/src/mappers"
	"doggystuffweb/src/repositories"
	"doggystuffweb/src/viewmodels"
	"html/template"
	"net/http"
)

const filesBasePath = "./files"

type home struct {
	homeTemplate *template.Template
}

func (h home) registerReoutes() {
	http.HandleFunc("/", h.handleHome)
	http.HandleFunc("/all/", h.handleHome)
}

func (h home) handleHome(w http.ResponseWriter, request *http.Request) {

	err, pagesDto := repositories.GetPages()

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	pages := []viewmodels.PageViewModel{}

	for _, element := range pagesDto {
		page := mappers.Map(element)
		pages = append(pages, page)
	}

	pagesViewModel := new(viewmodels.PagesViewModel)
	pagesViewModel.UserViewModel = viewmodels.NewUserViewModel(request)
	pagesViewModel.Pages = pages

	w.Header().Add("Content-Type", "text/html")

	err = h.homeTemplate.Execute(w, pagesViewModel)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
