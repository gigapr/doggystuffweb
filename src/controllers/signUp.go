package controllers

import (
	"doggystuffweb/src/events"
	"doggystuffweb/src/repositories"
	"doggystuffweb/src/utils"
	"doggystuffweb/src/viewmodels"
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
)

type UserCreatedEvent struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Email     string `json:"email"`
}

type signUpPage struct {
	signUpPageTemplate *template.Template
	eventsPublisher    *events.EventsPlublisher
}

func (s signUpPage) registerReoutes() {

	http.HandleFunc("/signup/", s.handleSignUp)
	http.HandleFunc("/signupPost", s.handleSignUpPost)
}

func (s signUpPage) handleSignUp(w http.ResponseWriter, request *http.Request) {
	w.Header().Add("Content-Type", "text/html")

	vm := new(viewmodels.SignUpViewModel)
	vm.UserViewModel = viewmodels.NewUserViewModel(request)

	err := s.signUpPageTemplate.Execute(w, vm)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (s signUpPage) handleSignUpPost(w http.ResponseWriter, request *http.Request) {

	w.Header().Add("Content-Type", "text/html")

	signUpViewModel := &viewmodels.SignUpViewModel{
		FirstName:            request.FormValue("firstName"),
		LastName:             request.FormValue("lastName"),
		Email:                request.FormValue("email"),
		Password:             request.FormValue("password"),
		ConfirmationPassword: request.FormValue("confirmPassword"),
		UserViewModel:        viewmodels.NewUserViewModel(request),
	}

	if signUpViewModel.IsValid() == false {
		if err := s.signUpPageTemplate.Execute(w, signUpViewModel); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	} else {

		hashedPassword, err := utils.Encrypt(signUpViewModel.Password)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		err = repositories.CreateUser(signUpViewModel.Email, signUpViewModel.FirstName, signUpViewModel.LastName, hashedPassword)

		if err != nil {
			panic(err)
		}

		utils.SetSession(signUpViewModel.Email, w)

		userCreatedEvent := &UserCreatedEvent{FirstName: signUpViewModel.FirstName, LastName: signUpViewModel.LastName}
		userCreatedEvent.Email = signUpViewModel.Email

		asJson, err := json.Marshal(userCreatedEvent)
		if err != nil {
			fmt.Printf("Error: %s", err)
			panic(err)
		}
		event := events.NewEvent("UserCreatedEvent", string(asJson), 1)
		s.eventsPublisher.PublishMessage(event)

		http.Redirect(w, request, "/", http.StatusSeeOther)
	}
}
