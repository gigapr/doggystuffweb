package controllers

import (
	"doggystuffweb/src/events"
	"doggystuffweb/src/repositories"
	"doggystuffweb/src/viewmodels"
	"encoding/json"
	"fmt"
	"net/http"
)

type savePage struct {
	eventsPublisher *events.EventsPlublisher
}

type PageSavedEvent struct {
	Title string `json:"title"`
	Body  string `json:"body"`
}

func (s savePage) registerReoutes() {
	http.HandleFunc("/save/", s.handleSavePage)
}

func (s savePage) handleSavePage(w http.ResponseWriter, request *http.Request) {

	pageViewModel := &viewmodels.PageViewModel{
		Title:         request.FormValue("title"),
		Body:          request.FormValue("body"),
		UserViewModel: viewmodels.NewUserViewModel(request),
	}
	var err error
	id := request.FormValue("id")
	if id == "" {
		err, id = repositories.CreatePage(pageViewModel.Title, pageViewModel.Body)
	} else {
		err = repositories.UpdatePage(id, pageViewModel.Title, pageViewModel.Body)
	}

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	pageSavedEvent := &PageSavedEvent{Title: pageViewModel.Title, Body: pageViewModel.Body}

	asJson, err := json.Marshal(pageSavedEvent)
	if err != nil {
		fmt.Printf("Error: %s", err)
		panic(err)
	}
	event := events.NewEvent("PageSavedEvent", string(asJson), 1)
	s.eventsPublisher.PublishMessage(event)

	http.Redirect(w, request, "/view/"+id, http.StatusFound)
}
