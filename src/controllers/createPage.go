package controllers

import (
	"doggystuffweb/src/viewmodels"
	"html/template"
	"net/http"
)

type createPage struct {
	createPageTemplate *template.Template
}

func (c createPage) registerReoutes() {
	http.HandleFunc("/new/", c.handleCreatePage)
}

func (c createPage) handleCreatePage(w http.ResponseWriter, request *http.Request) {

	w.Header().Add("Content-Type", "text/html")

	pageViewModel := new(viewmodels.PageViewModel)
	pageViewModel.UserViewModel = viewmodels.NewUserViewModel(request)

	err := c.createPageTemplate.Execute(w, pageViewModel)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
