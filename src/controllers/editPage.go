package controllers

import (
	"doggystuffweb/src/events"
	"doggystuffweb/src/mappers"
	"doggystuffweb/src/repositories"
	"doggystuffweb/src/viewmodels"
	"fmt"
	"html/template"
	"net/http"
	"path"
	"strconv"
)

type editPage struct {
	editPageTemplate *template.Template
	eventsPublisher  *events.EventsPlublisher
}

func (e editPage) registerReoutes() {
	http.HandleFunc("/edit/", e.handleEditPage)
}

func (e editPage) handleEditPage(w http.ResponseWriter, request *http.Request) {
	pageID, _ := strconv.Atoi(path.Base(request.URL.Path))

	err, pageDTO := repositories.GetPage(pageID)

	if err != nil {
		http.Redirect(w, request, fmt.Sprintf("/edit/%b", pageID), http.StatusFound)
		return
	}

	w.Header().Add("Content-Type", "text/html")

	pageViewModel := mappers.Map(pageDTO)
	pageViewModel.UserViewModel = viewmodels.NewUserViewModel(request)

	err = e.editPageTemplate.Execute(w, pageViewModel)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
