package controllers

import (
	"doggystuffweb/src/events"
	"html/template"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	homeController       home
	saveController       savePage
	createPageController createPage
	viewPageController   viewPage
	editPageController   editPage
	loginPageController  loginPage
	signUpController     signUpPage
)

func Startup(templates map[string]*template.Template, eventPublisher *events.EventsPlublisher) {

	homeController.homeTemplate = templates["all.html"]
	homeController.registerReoutes()

	saveController.eventsPublisher = eventPublisher
	saveController.registerReoutes()

	createPageController.createPageTemplate = templates["new.html"]
	createPageController.registerReoutes()

	viewPageController.viewPageTemplate = templates["view.html"]
	viewPageController.registerReoutes()

	editPageController.editPageTemplate = templates["edit.html"]
	editPageController.registerReoutes()

	loginPageController.loginPageTemplate = templates["login.html"]
	loginPageController.registerReoutes()

	signUpController.signUpPageTemplate = templates["signUp.html"]
	signUpController.eventsPublisher = eventPublisher
	signUpController.registerReoutes()

	http.Handle("/img/", http.FileServer(http.Dir("public")))
	http.Handle("/css/", http.FileServer(http.Dir("public")))

	http.Handle("/metrics", promhttp.Handler())
}
