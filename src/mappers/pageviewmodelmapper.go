package mappers

import (
	"doggystuffweb/src/repositories"
	"doggystuffweb/src/viewmodels"
)

func Map(p repositories.PageDTO) viewmodels.PageViewModel {

	r := viewmodels.PageViewModel{
		ID:          p.Page_id,
		Title:       p.Title,
		Body:        p.Body,
		DateCreated: p.Date_created,
	}

	return r
}
