package utils

import "strings"

func Contains(slice []string, s string) bool {
	for _, a := range slice {
		if strings.Contains(s, a) {
			return true
		}
	}
	return false
}
