package utils

import (
	"net/http"
	"time"

	"github.com/gorilla/securecookie"
)

var cookieHandler = securecookie.New(securecookie.GenerateRandomKey(64), securecookie.GenerateRandomKey(32))

func SetSession(userName string, response http.ResponseWriter) {
	value := map[string]string{
		"userName": userName,
	}

	if encoded, err := cookieHandler.Encode("session", value); err == nil {
		cookie := &http.Cookie{
			Name:     "session",
			Value:    encoded,
			Path:     "/",
			Expires:  time.Now().AddDate(0, 0, 1),
			HttpOnly: true,
		}
		http.SetCookie(response, cookie)
	}
}

func ClearSessionCookie(response http.ResponseWriter) {

	cookie := &http.Cookie{
		Name:     "session",
		Value:    "",
		MaxAge:   -1,
		Expires:  time.Unix(0, 0),
		HttpOnly: true,
	}

	http.SetCookie(response, cookie)
}

func GetUserName(request *http.Request) (userName string) {
	if cookie, err := request.Cookie("session"); err == nil {
		cookieValue := make(map[string]string)

		if err = cookieHandler.Decode("session", cookie.Value, &cookieValue); err == nil {
			userName = cookieValue["userName"]
		}
	}
	return userName
}
