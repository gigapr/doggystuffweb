package utils

import (
	"golang.org/x/crypto/bcrypt"
)

func Encrypt(password string) ([]byte, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.MinCost)

	return hashedPassword, err
}

func PasswordIsValid(hashedPassword string, password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))

	return err != nil
}
