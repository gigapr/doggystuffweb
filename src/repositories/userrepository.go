package repositories

import (
	"time"
)

func CreateUser(email string, firstName string, lastName string, password []byte) error {
	sqlStatement := ` INSERT INTO users (email, first_name, last_name, password, date_created)
					  VALUES ($1, $2, $3, $4, $5)`

	_, err := Db.Exec(sqlStatement, email, firstName, lastName, password, time.Now().UTC())

	return err
}

func GetUserPassword(username string) (error, string) {
	var password string

	sqlStatement := `SELECT password FROM users WHERE email=$1;`

	row := Db.QueryRow(sqlStatement, username)
	err := row.Scan(&password)

	return err, password
}
