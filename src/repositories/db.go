package repositories

import (
	"database/sql"
	"fmt"
	"log"
)

var Db *sql.DB

func ConnectToDatabase(host string, database string, port string, user string, password string) *sql.DB {

	connectionstring := "host=" + host + " dbname=" + database + " user=" + user + " password=" + password + " port=" + port + " sslmode=disable"

	db, err := sql.Open("postgres", connectionstring)

	if err != nil {
		log.Fatalln(fmt.Errorf("Unable to connect to database. %v", err))
	}

	Db = db

	return db
}
