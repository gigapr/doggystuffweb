package repositories

import "time"

type PageDTO struct {
	Page_id      int
	Title        string
	Body         string
	Date_created time.Time
}
