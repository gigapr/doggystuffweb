package repositories

import (
	"log"
	"time"
)

func CreatePage(title string, body string) (error, string) {
	sqlStatement := ` INSERT INTO pages (title, body, date_created)
					  VALUES ($1, $2, $3)
					  RETURNING page_id`

	id := ""
	err := Db.QueryRow(sqlStatement, title, body, time.Now().UTC()).Scan(&id)

	return err, id
}

func GetPages() (error, []PageDTO) {

	sqlStatement := `SELECT page_id, title, body, date_created FROM pages;`

	rows, err := Db.Query(sqlStatement)
	if err != nil {
		log.Fatalf("Query: %v", err)
	}

	pages := []PageDTO{}
	for rows.Next() {
		var r PageDTO
		err = rows.Scan(&r.Page_id, &r.Title, &r.Body, &r.Date_created)
		if err != nil {
			log.Fatalf("Scan: %v", err)
		}
		pages = append(pages, r)
	}

	return nil, pages
}

func GetPage(id int) (error, PageDTO) {
	var r PageDTO

	sqlStatement := `SELECT page_id, title, body, date_created FROM pages WHERE page_id=$1;`

	row := Db.QueryRow(sqlStatement, id)
	err := row.Scan(&r.Page_id, &r.Title, &r.Body, &r.Date_created)

	return err, r
}

func UpdatePage(id string, title string, body string) error {

	sqlStatement := `UPDATE pages SET title=$1, body=$2 WHERE page_id=$3;`

	_, error := Db.Exec(sqlStatement, title, body, id)

	return error
}
